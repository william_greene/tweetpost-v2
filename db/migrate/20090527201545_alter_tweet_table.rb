class AlterTweetTable < ActiveRecord::Migration
  def self.up
    execute "ALTER TABLE tweets MODIFY status_id BIGINT UNSIGNED UNIQUE"
    execute "ALTER TABLE tweets MODIFY status VARCHAR(255) CHARACTER SET utf8 default NULL"
  end

  def self.down
    execute "ALTER TABLE tweets MODIFY status_id INT"
    execute "ALTERT ABLE tweets MODIFY status varchar(255) default NULL"
  end
end
