class AddRecentFailuresToPostingConfigurations < ActiveRecord::Migration
  def self.up
    add_column :posting_configurations, :recent_failures, :integer
  end

  def self.down
    remove_column :posting_configurations, :recent_failures
  end
end
