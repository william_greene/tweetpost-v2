class AddActionLinkToFacebookPost < ActiveRecord::Migration
  def self.up
    add_column :facebook_posts, :action_link, :string
  end

  def self.down
    remove_column :facebook_posts, :action_link
  end
end
