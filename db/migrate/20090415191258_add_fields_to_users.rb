class AddFieldsToUsers < ActiveRecord::Migration
  def self.up
    add_column :users, :twitter_name, :string
    add_column :users, :explicitly_tagged, :boolean
    add_column :users, :use_real_names, :boolean, :default => true
    add_column :users, :post_as_notes, :boolean, :default => true
    add_column :users, :awesm_powered, :boolean
    add_column :users, :awesm_api_key, :string
    add_column :users, :awesm_user_id, :string
  end

  def self.down
    remove_column :users, :awesm_user_id
    remove_column :users, :awesm_api_key
    remove_column :users, :awesm_powered
    remove_column :users, :post_as_notes
    remove_column :users, :use_real_names
    remove_column :users, :explicitly_tagged
    remove_column :users, :twitter_name
  end
end
