class AddStatusCreatedAtToTweet < ActiveRecord::Migration
  def self.up
    add_column :tweets, :status_created_at, :timestamp
  end

  def self.down
    remove_column :tweets, :status_created_at
  end
end
