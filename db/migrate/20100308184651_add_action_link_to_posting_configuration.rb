class AddActionLinkToPostingConfiguration < ActiveRecord::Migration
  def self.up
    add_column :posting_configurations, :action_link, :boolean, :default => false
  end

  def self.down
    remove_column :posting_configurations, :action_link
  end
end
