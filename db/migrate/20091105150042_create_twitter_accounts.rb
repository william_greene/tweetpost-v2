class CreateTwitterAccounts < ActiveRecord::Migration
  def self.up
    create_table :twitter_accounts do |t|
      t.integer :user_id
      t.integer :twitter_user_id
      t.string :twitter_username
      t.string :twitter_real_name
      t.string :twitter_picture
      t.integer :last_tweet, :default => 0
      t.integer :twitter_private, :default => 0

      t.timestamps
    end
  end

  def self.down
    drop_table :twitter_accounts
  end
end
