class AddStateToTweet < ActiveRecord::Migration
  def self.up
    add_column :tweets, :state, :string, :default => 'received'
  end

  def self.down
    remove_column :tweets, :state
  end
end
