class AddSourceToTweet < ActiveRecord::Migration
  def self.up
    add_column :tweets, :source, :string
  end

  def self.down
    remove_column :tweets, :source
  end
end
