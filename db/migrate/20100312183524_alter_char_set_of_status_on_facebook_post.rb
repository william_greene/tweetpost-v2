class AlterCharSetOfStatusOnFacebookPost < ActiveRecord::Migration
  def self.up
    execute "ALTER TABLE facebook_posts CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci"
    execute "ALTER TABLE authorized_facebook_spots CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci"
    execute "ALTER TABLE posting_configurations CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci"
    execute "ALTER TABLE twitter_accounts CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci"
    execute "ALTER TABLE users CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci"
    execute "ALTER TABLE tweets CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci"
  end

  def self.down
  end
end
