class CreateFacebookPosts < ActiveRecord::Migration
  def self.up
    create_table :facebook_posts do |t|
      t.integer :tweet_id
      t.integer :posting_configuration_id
      t.integer :authorized_facebook_spot_id
      t.text :status
      t.text :attachment
      t.string :link
      t.string :awesm_link      
      t.string :state
      t.string :fb_link_id
      t.timestamps
    end
  end

  def self.down
    drop_table :facebook_posts
  end
end
