class CreateAuthorizedFacebookSpots < ActiveRecord::Migration
  def self.up
    create_table :authorized_facebook_spots do |t|
      t.integer :user_id
      t.string :title
      t.string :page_type
      t.string :fb_id
      t.string :fb_url
      t.string :fb_picture
      t.timestamps
    end
  end

  def self.down
    drop_table :authorized_facebook_spots
  end
end
