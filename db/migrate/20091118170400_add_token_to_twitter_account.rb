class AddTokenToTwitterAccount < ActiveRecord::Migration
  def self.up
    add_column :twitter_accounts, :oauth_token, :string
    add_column :twitter_accounts, :oauth_verifier, :string
  end

  def self.down
    remove_column :twitter_accounts, :oauth_token
    remove_column :twitter_accounts, :oauth_verifier
  end
end