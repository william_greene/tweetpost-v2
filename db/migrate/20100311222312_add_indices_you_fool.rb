class AddIndicesYouFool < ActiveRecord::Migration
  def self.up
    add_index :authorized_facebook_spots, :user_id
    add_index :facebook_posts, :tweet_id
    add_index :facebook_posts, :posting_configuration_id
    add_index :facebook_posts, :authorized_facebook_spot_id
    add_index :facebook_posts, :state
    add_index :posting_configurations, :user_id
    add_index :posting_configurations, :twitter_account_id
    add_index :posting_configurations, :authorized_facebook_spot_id
    add_index :posting_configurations, :disable_tweetface
    add_index :tweets, :state
    add_index :tweets, :twitter_user_id
    add_index :twitter_accounts, :user_id
    add_index :twitter_accounts, :twitter_user_id
    add_index :twitter_accounts, :polling
  end

  def self.down
    remove_index :twitter_accounts, :polling
    remove_index :twitter_accounts, :twitter_user_id
    remove_index :twitter_accounts, :user_id
    remove_index :tweets, :twitter_user_id
    remove_index :tweets, :state
    remove_index :posting_configurations, :disable_tweetface
    remove_index :posting_configurations, :authorized_facebook_spot_id
    remove_index :posting_configurations, :twitter_account_id
    remove_index :posting_configurations, :user_id
    remove_index :facebook_posts, :state

    remove_index :facebook_posts, :authorized_facebook_spot_id
    remove_index :facebook_posts, :posting_configuration_id
    remove_index :facebook_posts, :tweet_id
    remove_index :authorized_facebook_spots, :user_id
    mind
  end
end
