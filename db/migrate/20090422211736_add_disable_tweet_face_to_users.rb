class AddDisableTweetFaceToUsers < ActiveRecord::Migration
  def self.up
    add_column :users, :disable_tweetface, :boolean, :default => false
  end

  def self.down
    remove_column :users, :disable_tweetface
  end
end
