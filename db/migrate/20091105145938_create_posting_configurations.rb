class CreatePostingConfigurations < ActiveRecord::Migration
  def self.up
    create_table :posting_configurations do |t|
      t.integer :user_id
      t.integer :twitter_account_id
      t.integer :authorized_facebook_spot_id
      t.string :title
      t.boolean :explicitly_tagged
      t.boolean :use_real_names, :default => true
      t.boolean :post_as_notes, :default => true
      t.boolean :awesm_powered
      t.string :awesm_api_key
      t.string :awesm_user_id
      t.string :awesm_username
      t.boolean :disable_tweetface, :default => false
      t.timestamps
    end
  end

  def self.down
    drop_table :posting_configurations
  end
end
