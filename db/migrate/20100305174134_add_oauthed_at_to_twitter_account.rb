class AddOauthedAtToTwitterAccount < ActiveRecord::Migration
  def self.up
    add_column :twitter_accounts, :oauthed_at, :timestamp
  end

  def self.down
    remove_column :twitter_accounts, :oauthed_at
  end
end
