class AddSessionToUser < ActiveRecord::Migration
  def self.up
    add_column :users, :fb_session_key, :string
    add_column :users, :fb_session_secret_key, :string
  end

  def self.down
    remove_column :users, :fb_session_key
    remove_column :users, :fb_session_secret_key
  end
end
