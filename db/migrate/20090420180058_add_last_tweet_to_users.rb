class AddLastTweetToUsers < ActiveRecord::Migration
  def self.up
    add_column :users, :last_tweet, :integer, :default => 0
  end

  def self.down
    remove_column :users, :last_tweet
  end
end
