class AddTweetTableForActiveRecordVersionOfModel < ActiveRecord::Migration
  def self.up
    create_table :tweets, :force => true do |t|
      t.string :status
      t.integer :prefs_id
      t.integer :reply_to
      t.integer :status_id
      t.timestamps
    end
    add_index :tweets, [:prefs_id, :status_id], :unique => true
  end

  def self.down
    remove_index :tweets, :column => [:prefs_id, :status_id]
    drop_table :tweets
  end
end
