class AddPollingToTwitterAccount < ActiveRecord::Migration
  def self.up
    add_column :twitter_accounts, :polling, :boolean, :default => true
  end

  def self.down
    remove_column :twitter_accounts, :polling
  end
end
