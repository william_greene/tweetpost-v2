# This file is auto-generated from the current state of the database. Instead of editing this file, 
# please use the migrations feature of Active Record to incrementally modify your database, and
# then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your database schema. If you need
# to create the application database on another system, you should be using db:schema:load, not running
# all the migrations from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended to check this file into your version control system.

ActiveRecord::Schema.define(:version => 20100311222312) do

  create_table "authorized_facebook_spots", :force => true do |t|
    t.integer  "user_id"
    t.string   "title"
    t.string   "page_type"
    t.string   "fb_id"
    t.string   "fb_url"
    t.string   "fb_picture"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "authorized_facebook_spots", ["user_id"], :name => "index_authorized_facebook_spots_on_user_id"

  create_table "facebook_posts", :force => true do |t|
    t.integer  "tweet_id"
    t.integer  "posting_configuration_id"
    t.integer  "authorized_facebook_spot_id"
    t.text     "status"
    t.text     "attachment"
    t.string   "link"
    t.string   "awesm_link"
    t.string   "state"
    t.string   "fb_link_id"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "action_link"
  end

  add_index "facebook_posts", ["authorized_facebook_spot_id"], :name => "index_facebook_posts_on_authorized_facebook_spot_id"
  add_index "facebook_posts", ["posting_configuration_id"], :name => "index_facebook_posts_on_posting_configuration_id"
  add_index "facebook_posts", ["state"], :name => "index_facebook_posts_on_state"
  add_index "facebook_posts", ["tweet_id"], :name => "index_facebook_posts_on_tweet_id"

  create_table "posting_configurations", :force => true do |t|
    t.integer  "user_id"
    t.integer  "twitter_account_id"
    t.integer  "authorized_facebook_spot_id"
    t.string   "title"
    t.boolean  "explicitly_tagged"
    t.boolean  "use_real_names",              :default => true
    t.boolean  "post_as_notes",               :default => true
    t.boolean  "awesm_powered"
    t.string   "awesm_api_key"
    t.string   "awesm_user_id"
    t.string   "awesm_username"
    t.boolean  "disable_tweetface",           :default => false
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "recent_failures"
    t.boolean  "action_link",                 :default => false
  end

  add_index "posting_configurations", ["authorized_facebook_spot_id"], :name => "index_posting_configurations_on_authorized_facebook_spot_id"
  add_index "posting_configurations", ["disable_tweetface"], :name => "index_posting_configurations_on_disable_tweetface"
  add_index "posting_configurations", ["twitter_account_id"], :name => "index_posting_configurations_on_twitter_account_id"
  add_index "posting_configurations", ["user_id"], :name => "index_posting_configurations_on_user_id"

  create_table "tweets", :force => true do |t|
    t.string   "status"
    t.integer  "prefs_id"
    t.integer  "reply_to"
    t.integer  "status_id",         :limit => 8
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "state",                          :default => "received"
    t.string   "twitter_user_id"
    t.string   "source"
    t.datetime "status_created_at"
  end

  add_index "tweets", ["prefs_id", "status_id"], :name => "index_tweets_on_prefs_id_and_status_id", :unique => true
  add_index "tweets", ["state"], :name => "index_tweets_on_state"
  add_index "tweets", ["status_id"], :name => "status_id", :unique => true
  add_index "tweets", ["twitter_user_id"], :name => "index_tweets_on_twitter_user_id"

  create_table "twitter_accounts", :force => true do |t|
    t.integer  "user_id"
    t.integer  "twitter_user_id"
    t.string   "twitter_username"
    t.string   "twitter_real_name"
    t.string   "twitter_picture"
    t.integer  "last_tweet",        :default => 0
    t.integer  "twitter_private",   :default => 0
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "oauth_token"
    t.string   "oauth_verifier"
    t.datetime "oauthed_at"
    t.boolean  "polling",           :default => true
  end

  add_index "twitter_accounts", ["polling"], :name => "index_twitter_accounts_on_polling"
  add_index "twitter_accounts", ["twitter_user_id"], :name => "index_twitter_accounts_on_twitter_user_id"
  add_index "twitter_accounts", ["user_id"], :name => "index_twitter_accounts_on_user_id"

  create_table "users", :force => true do |t|
    t.string   "login",                     :limit => 40
    t.string   "name",                      :limit => 100, :default => ""
    t.string   "email",                     :limit => 100
    t.string   "crypted_password",          :limit => 40
    t.string   "salt",                      :limit => 40
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "remember_token",            :limit => 40
    t.datetime "remember_token_expires_at"
    t.integer  "fb_user_id",                :limit => 8
    t.string   "email_hash"
    t.string   "twitter_name"
    t.boolean  "explicitly_tagged"
    t.boolean  "use_real_names",                           :default => true
    t.boolean  "post_as_notes",                            :default => true
    t.boolean  "awesm_powered"
    t.string   "awesm_api_key"
    t.string   "awesm_user_id"
    t.integer  "last_tweet",                               :default => 0
    t.boolean  "disable_tweetface",                        :default => false
    t.string   "fb_session_key"
    t.string   "fb_session_secret_key"
  end

  add_index "users", ["login"], :name => "index_users_on_login", :unique => true

end
