class AuthorizedFacebookSpot < ActiveRecord::Base
  belongs_to :user
  has_many :posting_configurations
  has_many :facebook_posts
end
