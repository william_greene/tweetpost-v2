class PostingConfiguration < ActiveRecord::Base
  include Awesm
  belongs_to :authorized_facebook_spot
  belongs_to :twitter_account
  belongs_to :user
  has_many :facebook_posts
  named_scope :enabled, :conditions => { 'disable_tweetface' => false }
  
  validates_presence_of :user, :title, :twitter_account, :authorized_facebook_spot
  validates_uniqueness_of :user_id, :scope => [:twitter_account_id, :authorized_facebook_spot_id], :message => "You can only connect a particular Twitter account to a particular Facebook profile or fan page once. Please choose a different Twitter account or fan page/profile."
  
  
  # def apply_preferences
  #   if twitter_accounts.nil?
  #     fail!
  #     puts "FAILING this tweet - no matching twitter accounts found"
  #     return false
  #   else
  #     posting_configurations = twitter_accounts.collect { |ta| ta.posting_configurations }
  #     puts posting_configurations.inspect
  #     if posting_configurations.nil?
  #       fail!
  #       puts "FAILING this tweet - no matching posting configurations found"
  #       return false        
  #     else
  #       posting_configurations.each do |posting_config|
  #       puts "in a posting config - #{status}"
  #         unless posting_config.nil?
  #           posting_config.each do |pconfig|
  #             puts "testing another posting config - #{status}"
  #             if pconfig.explicitly_tagged
  #               puts "configuration requires explicitly tagged status to post"
  #               post(pconfig) if (status =~ /#fb/i)
  #             else
  #               puts "configuration does not require explicitly tagged status to post"
  #               post(pconfig) if (reply_to.blank? && (status !~ /\!fb/i)) || (status =~ /#fb/i)
  #             end
  #           end
  #         end
  #       end
  #     end
  #     puts "DELIVERY! - delivering this tweet, it is DONE, it has been delivered to every location"
  #     deliver!
  #   end
  # end
  
  def fb_user
    # TODO: this a guard to check if the session is invalid, not just nil?
    return @fb_user unless @fb_user.nil?
    @fb_user = Facebooker::User.new(user.fb_user_id)
    @fb_user.session = Facebooker::Session.create(FACEBOOK_API_KEY,FACEBOOK_API_SECRET)
    @fb_user.populate
    @fb_user.session.instance_variable_set("@session_key", user.fb_session_key) unless user.fb_session_key.blank? 
    @fb_user
  end
  
  def postable?(tweet)
    return true if tweet.status =~ /#fb/i
    return false if explicitly_tagged
    return false if tweet.status =~ /\!fb/i
    return false unless tweet.reply_to.blank?
    true
  end

  def action_link_url
    return "" unless self.action_link
    "http://twitter.com/#{twitter_account.twitter_username}"
  end


  def post(tweet)
    return unless postable?(tweet)
    puts "#{Time.now} - tweet #{tweet.status_id} is postable"    
    @postable_status = tweet.status.dup
    
    remove_fb_tag
    humanize_names if use_real_names
    
    extract_links
    replace_links if awesm_powered?
    if post_links_to_wall and not @links.empty?
      puts "found #{@links.count} links to post"
      
      @links.each do |link|
        f = self.facebook_posts.create :status => @postable_status, :link => link[0], :awesm_link => link[1],
                              :authorized_facebook_spot => authorized_facebook_spot, :tweet => tweet, :action_link => action_link_url
      end
    else
      puts "No links, created post"
      f = self.facebook_posts.create :status => @postable_status, :authorized_facebook_spot => authorized_facebook_spot, :tweet => tweet, :action_link => action_link_url
    end
    puts "#{Time.now} - created fb posting #{f.inspect}"
    
    twitter_account.last_tweet = tweet.id
    twitter_account.save
  end
  
  def has_valid_twitter_account?
    if twitter_account.has_oauth_tokens?
      return false unless twitter_account.verify_with_oauth?
    else
      return false unless twitter_account.verify_without_oauth?
    end
    true
  end

  def remove_fb_tag
    @postable_status.gsub!(/#fb/i,' ') and @postable_status.gsub!(/\!fb/i,' ')
  end
  
  def humanize_names
    handles = @postable_status.scan(/\@\w+/)
    not handles.collect { |handle| @postable_status.gsub!(handle, humanize(handle[1..-1])) }.member? nil
  end
  
  def humanize handle
    HTMLEntities.new.decode((Hpricot(open("http://twitter.com/users/show.xml?screen_name=#{handle}")) / "name").inner_html)
  rescue StandardError, Timeout::Error
    puts "TWITTER ERROR: failed on screen name look up in humanize"
    "@#{handle}"
  end
  
  def extract_links
    @links = []
    URI.extract(@postable_status, %w(http https ftp sftp mailto im aim)) do |link|
      begin
        link = link.match(TWITTER_REGEX).to_a[2..3].join
      rescue
        puts "error in link extraction, #{link} did not match the twitter regex"
      else
        @links << [link, link]
      end
    end
  end
  
  def create_awesm_links
    @links.collect! do |link|
      [link[0], awesmate(link[1], user_params)["awesm_url"]]
    end    
  end
  
  def replace_links
    return false if @links.empty?
    create_awesm_links
    @links.each do |link|
      @postable_status = @postable_status.gsub(/#{link[0].gsub('?','\?')}([\.\,\)\]\}\!\?\-\=\+\~\>\<]*($|\ +))/, link[1] + '\1')
    end
  end
  
  def user_params
    { "share_type" => (post_links_to_wall ? 'facebook-post' : 'facebook-status'), 
                   "api_key" => awesm_api_key, "user_id" => "facebook:"+authorized_facebook_spot.fb_id }
  end
  
  def sad_panda!
    self.recent_failures += 1
    self.disable_tweetface = true if recent_failures >= 15
    save
  end
  
  def woot!
    self.recent_failures = 0
    save
  end
  
  # if awesm_powered
  #   # puts "should be awesming"
  #   @links = @links.collect! do |link|
  #     response = awesmate link[0]
  #     puts "\n\n#{response.inspect}\n\nnew awesm url created #{response["awesm_url"]}"
  #     @postable_status = @postable_status.gsub(/#{link[0].gsub('?','\?')}([\.\,\)\]\}\!\?\-\=\+\~\>\<]*($|\ +))/, response["awesm_url"] + '\1')
  #     [response["original_url"], response["awesm_url"]]
  #   end
  # end
  
  
  def post_links_to_wall
    post_as_notes
  end
  
end
