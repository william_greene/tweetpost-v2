class FacebookPost < ActiveRecord::Base
  include AASM
  include Awesm
  include SelectivelyLog
  
  belongs_to :posting_configuration
  belongs_to :tweet
  belongs_to :authorized_facebook_spot  
  
  aasm_column :state
  aasm_initial_state :received
  aasm_state :received
  aasm_state :publishing_attempt1
  aasm_state :publishing_attempt2
  aasm_state :publishing_attempt3  
  aasm_state :previewing_attempt1
  aasm_state :previewing_attempt2
  aasm_state :previewing_attempt3  
  aasm_state :previewed
  aasm_state :delivered  
  aasm_state :failed        
  
  aasm_event :deliver do
    transitions :from => [:publishing_attempt1, :publishing_attempt2, :publishing_attempt3], :to => :delivered
  end
  
  aasm_event :publish_attempt do
    transitions :from => [:previewed, :received], :to => :publishing_attempt1
    transitions :from => [:publishing_attempt1], :to => :publishing_attempt2
    transitions :from => [:publishing_attempt2], :to => :publishing_attempt3
    transitions :from => [:publishing_attempt3], :to => :failed
  end
      
  aasm_event :accept_preview do
    transitions :from => [:previewing_attempt1, :previewing_attempt2, :previewing_attempt3], :to => :previewed
  end
  
  aasm_event :preview_attempt do
    transitions :from => [:received], :to => :previewing_attempt1    
    transitions :from => [:previewing_attempt1], :to => :previewing_attempt2    
    transitions :from => [:previewing_attempt2], :to => :previewing_attempt3
    transitions :from => [:previewing_attempt3], :to => :publishing_attempt1
  end
  
  aasm_event :fail do
    transitions :from => [:previewing_attempt3, :publishing_attempt3, :failed], :to => :failed
  end

  def attemptable?
    return true if received?
    return true if ((previewing_attempt1? || publishing_attempt1?) && updated_at < 1.minute.ago)
    return true if ((previewing_attempt2? || publishing_attempt2?) && updated_at < 10.minutes.ago)
    false
  end
    
  def fb_user
    posting_configuration.fb_user
  end
  
  
  # if awesm_powered
  #   # puts "should be awesming"
  #   @links = @links.collect! do |link|
  #     response = awesmate link[0]
  #     puts "\n\n#{response.inspect}\n\nnew awesm url created #{response["awesm_url"]}"
  #     @postable_status = @postable_status.gsub(/#{link[0].gsub('?','\?')}([\.\,\)\]\}\!\?\-\=\+\~\>\<]*($|\ +))/, response["awesm_url"] + '\1')
  #     [response["original_url"], response["awesm_url"]]
  #   end
  # end
  
  def remove_other_link_tweets
    #return true if links.count <= 1
    all_posts = FacebookPost.find_all_by_tweet_id_and_posting_configuration_id(tweet.id, posting_configuration.id)
    all_posts.map &:fail!
  end
  
  def links
    @links ||= URI.extract(status, %w(http https ftp sftp mailto im aim)).collect do |url|
      url.match(TWITTER_REGEX).to_a[2..3].join
    end.compact
  end
  
  def action_link_hash
    return "" if self.action_link.blank?
    {:text => "Follow me on Twitter", :href => "http://twitter.com/#{posting_configuration.twitter_account.twitter_username}"}
  end  
  
  # TODO: log intelligently.
  def publish!
    puts "checking if publish it attemptable?"
    if attemptable?
      puts "its attemptable"
      if link
        puts link
        if received? or previewing_attempt1? or previewing_attempt2?
          preview_attempt!
          slog(posting_configuration.user_id, "#{tweet.status_id} new preview status: #{tweet.state}")
          
          self.attachment = preview(link, awesm_link)
          if valid_attachment?
            accept_preview!
          elsif previewing_attempt3?
            fail!
            slog(posting_configuration.user_id, "#{tweet.status_id} has FAILED to preview after 3 attempts.")
            
            remove_other_link_tweets
            FacebookPost.create(:posting_configuration => posting_configuration, :status => status,
                        :authorized_facebook_spot => authorized_facebook_spot, :tweet => tweet, :action_link => action_link)
          end
        end     
      end       
        
      if previewed? or received? or publishing_attempt1? or publishing_attempt2?
        puts "we are at attemptable publish" 
        publish_attempt!
        slog(posting_configuration.user_id, "#{tweet.status_id} new publish status: #{tweet.state}")
        
        puts "state advanced"
        api_params = {:message => status, :uid => authorized_facebook_spot.fb_id, :attachment => Facebooker.json_encode(attachment)}
        api_params["action_links"] = [action_link_hash] unless action_link_hash.blank?
        api = fb_user.session.post('facebook.stream.publish', api_params, false)
        # slog(posting_configuration.user_id, "#{tweet.status_id} response code from publishing attempt to Facebook: #{api.inspect}")
        puts api.inspect
        # TODO: write api.good? (think this returns just the ID when good, returns hash)
        # TODO: save final link id (add this to the model too! - you cant save this in an integer its huge)
        if api.class == String
          self.fb_link_id = api
          deliver!
          slog(posting_configuration.user_id, "#{tweet.status_id} has been delivered.")
          posting_configuration.woot!
        else
          slog(posting_configuration.user_id, "#{tweet.status_id} was not delivered.") # TODO: Add error code
          posting_configuration.sad_panda!
        end
      end
      fail! if publishing_attempt3?
    else
      puts "this is not attemptable #{Time.now} | #{updated_at}"
    end
 
    rescue StandardError => e
      #if tweet
      #  slog(0, "User #{posting_configuration.user_id} - Tweet status id '#{tweet.status_id}' has failed to publish. State: #{tweet.state}")
      #end
      puts "ERROR: STANDARD ERROR - We have a standard error\n#{Time.now}\n"
      puts "facebook_post state: #{state}\nfb api return: #{api.inspect}\n"      
      puts "sending: #{status}\nattachment: #{attachment.inspect}\nlink: #{link} - awesm_link: #{awesm_link}\n"
      if posting_configuration
#	posting_configuration.sad_panda!
#	if publishing_attempt3?
#		fail!
#	end

        puts "for: @#{posting_configuration.twitter_account.twitter_username}\nto: #{authorized_facebook_spot.inspect}\n"
        puts "USER: #{posting_configuration.user.inspect}\n\nORIGINAL TWEET: #{tweet.inspect}\n"      
        puts "POSTING CONFIGURATION: #{posting_configuration.inspect}\n\n\n"
      else
        puts "no posting configuration, - edge case - prolly deleted by user"  
      end
      puts e.inspect
      puts e.message.to_s
      puts e.backtrace
    rescue TimeoutError => e
      puts "ERROR: TIMEOUT ERROR - We have a timeout error\n#{Time.now}\n"
      puts "facebook_post state: #{state}\nfb api return: #{api.inspect}\n"      
      puts "sending: #{status}\nattachment: #{attachment.inspect}\nlink: #{link} - awesm_link: #{awesm_link}\n"
      if posting_configuration
        puts "for: @#{posting_configuration.twitter_account.twitter_username}\nto: #{authorized_facebook_spot.inspect}\n"
        puts "USER: #{posting_configuration.user.inspect}\n\nORIGINAL TWEET: #{tweet.inspect}\n"      
        puts "POSTING CONFIGURATION: #{posting_configuration.inspect}\n\n\n"
      else
        puts "no posting configuration, - edge case - prolly deleted by user"  
      end
      puts e.inspect
      puts e.message.to_s
      puts e.backtrace
      
  end
  #get new posts (received), attempt1 posts older than 1 minute, attempt2 posts older than 10 minutes
  
  # states: received, attempt1, attempt2, attempt3, delivered, error-session-key, error-timeout, error-other, failed
  
  # TODO: which spot did it go to?
  
  # TODO: add field to save ID of post on facebook

#private
  def valid_attachment?
    #not (attachment["media"].blank? or attachment["description"].blank? or attachment.has_key? "error_code")
    validity = !(attachment["media"].blank? or attachment["description"].blank? or attachment.has_key? "error_code")
    puts "Attachment is valid? #{validity}"
    validity
  end  

  def preview(url, awesm_url)
    self.attachment = Hash.new
    preview = fb_user.session.post('facebook.links.preview',{:url => url, :format => 'json'}, true)
    # puts "\nPREVIEW LINK RETURN #{preview.body} return code#{preview.code} #{preview.code.class}\n"
    slog(posting_configuration.user_id, "#{tweet.status_id} response from preview attempt to Facebook: #{preview.code.to_i}")
    if preview.code.to_i == 200
      self.attachment = Facebooker.json_decode(preview.body)
      self.attachment["href"] = awesm_url
      if self.attachment["media"]
        self.attachment["media"].each {|x| x['href'] = awesm_url if x['href'].blank? }
      end
    end
    puts "#{Time.now} - previewed: #{self.attachment}"
    self.attachment
  end
  
  
end
