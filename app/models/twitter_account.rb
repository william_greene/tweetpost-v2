class TwitterAccount < ActiveRecord::Base
  belongs_to :user
  has_many :posting_configurations
  has_many :tweets, :foreign_key => "twitter_user_id", :primary_key => "twitter_user_id"
  validates_uniqueness_of :twitter_user_id, :scope => :user_id, :message => "You have already added that Twitter user."


  def enabled_configs
    posting_configurations.enabled
  end
  
  def self.last_user_made_at
    TwitterAccount.last.created_at
  end

  def new?
    tweets.count == 0
  end

  def has_enabled_configuration?
    !!posting_configurations.count(:conditions => 'disable_tweetface = false')
  end

  def has_oauth_tokens?
    not (oauth_token.blank? || oauth_verifier.blank?)
  end
  
  def verify_with_oauth?
    client = TwitterOAuth::Client.new(:consumer_key => TWITTER_CONSUMER_KEY, :consumer_secret => TWITTER_CONSUMER_SECRET, :token => oauth_token, :secret =>oauth_verifier)
    user_info = client.info
    return false if user_info.has_key? "error"
    true
  end
  
  def verify_without_oauth?
    non_oauth_client = Twitter::Base.new(Twitter::HTTPAuth.new(TWITTER_MASTER_USER, TWITTER_MASTER_PASS))
    user_info = non_oauth_client.user twitter_user_id
    return false if user_info["protected"] == "true"
    true
  end

  def disable_non_oauthed_accounts
    TwitterAccount.find_all_by_twitter_user_id(twitter_user_id).select do |ta|
      ta.user_id != user_id && ta.oauth_token.blank?
    end.each do |ta|
      ta.posting_configurations.each do |pc|
        pc.disable_tweetface = true
        pc.save
      end
    end
  end

 end
