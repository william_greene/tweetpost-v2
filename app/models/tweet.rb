class Tweet < ActiveRecord::Base
  include AASM

  # this is the official twitter regex for url extraction.


  has_many :twitter_accounts, :foreign_key => "twitter_user_id", :primary_key => "twitter_user_id"
  has_many :posting_configurations, :through => :twitter_accounts
  has_many :facebook_posts
  attr_accessor :links
  
  validates_uniqueness_of :status_id
    
  aasm_column :state
  aasm_initial_state :received
  aasm_state :received
  aasm_state :processing
  aasm_state :delivered
  aasm_state :filtered
  aasm_state :doa
  aasm_state :error
  aasm_state :deleted
  
  aasm_event :process do
    transitions :from => [:received], :to => :processing
  end
  
  aasm_event :dead_on_arrival do
    transitions :from => [:received], :to => :doa
  end

  aasm_event :deleted_on_twitter do
    transitions :from => [:received], :to => :deleted
  end

  aasm_event :deliver do
    transitions :from => [:processing], :to => :delivered
  end

  aasm_event :filter do
    transitions :from => [:processing], :to => :filtered
  end

  aasm_event :fail do
    transitions :from => [:processing], :to => :error
  end

  aasm_event :restart do
    transitions :from => [:error], :to => :received
  end
  
  def self.create_from(mash)
    t_a = TwitterAccount.find_by_twitter_user_id(mash.user[:id])
    return false if t_a.nil?    
    return false if PostingConfiguration.find_by_twitter_account_id(t_a.id).nil?    
    tweet = Tweet.create do |t|
      t.status_id = mash[:id]
      t.twitter_user_id = mash.user[:id]
      t.reply_to = mash[:in_reply_to_user_id]
      t.status = HTMLEntities.new.decode(mash[:text])
    end
    tweet.save
     puts "Saved the status: #{tweet.status}"
  end

  def self.find_or_create_directly_polled_status(mash)
    tweet = Tweet.find_by_status_id(mash[:id])
    unless tweet
      tweet = Tweet.create do |t|
        t.status_id = mash[:id]
        t.twitter_user_id = mash.user[:id]
        t.reply_to = mash[:in_reply_to_user_id]
        t.status = HTMLEntities.new.decode(mash[:text])
        puts "tweet object created"
      end
      tweet.save
    else
      #break to avoid processing older tweets for this user
      puts "not creating: tweet already in system"
    end

  end
  
  def apply_preferences
    if twitter_accounts.nil?
      fail!
      puts "FAILING this tweet - no matching twitter accounts found"
      return false
    else
      posting_configurations = twitter_accounts.collect { |ta| ta.posting_configurations }
      puts posting_configurations.inspect
      if posting_configurations.nil?
        fail!
        puts "FAILING this tweet - no matching posting configurations found"
        return false        
      else
        posting_configurations.each do |posting_config|
        puts "in a posting config - #{status}"
          unless posting_config.nil?
            posting_config.each do |pconfig|
              puts "testing another posting config - #{status}"
              if pconfig.explicitly_tagged
                puts "configuration requires explicitly tagged status to post"
                post(pconfig) if (status =~ /#fb/i)
              else
                puts "configuration does not require explicitly tagged status to post"
                post(pconfig) if (reply_to.blank? && (status !~ /\!fb/i)) || (status =~ /#fb/i)
              end
            end
          end
        end
      end
      puts "DELIVERY! - delivering this tweet, it is DONE, it has been delivered to every location"
      deliver!
    end

    
  end

  def extract_and_awesm_links(posting_config)
    #TODO: could probably split this, extract the links in one go, and do it only once, 
    #      so we don't extract them everytime we process the tweet
    #      (which we may do multiple times because now a tweet may belong to multiple posting configurations)
    @links = []
    URI.extract(@postable_status, %w(http https ftp sftp mailto im aim)) do |link|
      # old regex: /[\.\,\)\]\}\!\?\-\=\+\~\>\<]$/
      link = link.match(TWITTER_REGEX).to_a[2..3].join
      @links << [link, link]
    end
    puts @links.inspect
    if posting_config.awesm_powered
      # puts "should be awesming"
      @links = @links.collect! do |link|
        response = posting_config.awesmate link[0]
        puts "\n\n#{response.inspect}\n\nnew awesm url created #{response["awesm_url"]}"
        @postable_status = @postable_status.gsub(/#{link[0]}([\.\,\)\]\}\!\?\-\=\+\~\>\<]*($|\ +))/, response["awesm_url"] + '\1')
        [response["original_url"], response["awesm_url"]]
      end
    end
puts @links.inspect
    @links = Hash[*@links.flatten]
    @links.inspect
  end

  def post(posting_config)
  puts "in post"
    user = Facebooker::User.new(posting_config.twitter_account.user.fb_user_id)
    user.session = Facebooker::Session.create(FACEBOOK_API_KEY,FACEBOOK_API_SECRET)
    user.populate
    @postable_status = status.dup
    #puts "status posting: #{@postable_status}"
    remove_fb_tag if posting_config.explicitly_tagged or (status =~ /#fb/i)
    humanize_names if posting_config.use_real_names
    extract_and_awesm_links posting_config
    #puts @postable_status
    if posting_config.post_as_notes and not @links.empty?
        #puts "posting links - session key #{posting_config.twitter_account.user.fb_session_key}"
        user.session.instance_variable_set("@session_key", posting_config.twitter_account.user.fb_session_key) unless posting_config.twitter_account.user.fb_session_key.blank?
        puts user.session.inspect
        @links.each_pair do |original, link|
          attachment = Hash.new

          valid_attachment = Proc.new do |a|
            (not a["media"].blank?) and (not a["description"].blank?)
          end

          retry_until(valid_attachment, :tries => 3) do
            preview = user.session.post('facebook.links.preview',{:url => original, :format => 'json'}, true)
            puts "\nPREVIEW LINK RETURN #{preview.body} return code#{preview.code} #{preview.code.class}\n"
            if preview.code.to_i == 200
              attachment = Facebooker.json_decode(preview.body)
              if attachment["media"]
                attachment["media"].each {|x| x['href'] = link if x['href'].blank? }
              end
            end
            attachment
          end

          if attachment and not attachment.has_key? "error_code"
         #    puts "ATTACHMENT #{attachment.inspect}"
             ret = user.session.post('facebook.stream.publish', {:message => @postable_status, :uid => posting_config.authorized_facebook_spot.fb_id, :attachment => Facebooker.json_encode(attachment)}, false)
          else
             puts "FAIL! = did not get link attachment - POSTING LINK WITHOUT LINK ATTACHMENT"
             ret = user.session.post('facebook.stream.publish', {:message => @postable_status, :uid => posting_config.authorized_facebook_spot.fb_id}, false)
          end 
          #ret = user.post_link(link,@postable_status)
          puts "RETURN VALUE link post return value #{ret.inspect}"
        end
    else
      
    #  if posting_config.authorized_facebook_spot.fb_id.to_i != posting_config.twitter_account.user.fb_user_id.to_i
        puts "posting status without links | spot:  #{posting_config.authorized_facebook_spot.fb_id} user:  #{posting_config.twitter_account.user.fb_user_id}"
        ret =  user.session.post('facebook.stream.publish', {:message => @postable_status, :uid => posting_config.authorized_facebook_spot.fb_id},false)
        puts "RETURN VALUE status post return value #{ret.inspect}"
    #  else
    #   puts "posting to a profile"
    #   ret =  user.publish_to(user,  :message => @postable_status)
    #   puts "RETURN VALUE profile status post return value #{ret.inspect}"
    #  end
    end
    posting_config.twitter_account.last_tweet = status_id && posting_config.twitter_account.save
  end
  
  def remove_fb_tag
    @postable_status.gsub!(/#fb/i,' ') and @postable_status.gsub!(/\!fb/i,' ')
  end

  def humanize_names
    handles = @postable_status.scan(/\@\w+/)
    not handles.collect { |handle| @postable_status.gsub!(handle, humanize(handle[1..-1])) }.member? nil
  end
  
  def humanize handle
     HTMLEntities.new.decode((Hpricot(open("http://twitter.com/users/show.xml?screen_name=#{handle}")) / "name").inner_html)
  end
end
