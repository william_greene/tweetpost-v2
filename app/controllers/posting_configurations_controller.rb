class PostingConfigurationsController < ApplicationController
  
  def destroy
    @posting_configuration = PostingConfiguration.find(params[:id])
    if @posting_configuration.user == current_user
      @posting_configuration.destroy
    else
      flash[:error] = "You can not edit a connection you do not own."
    end
    respond_to do |format|
      format.html { redirect_to("/settings") }
    end
  end
  
  # step 1
  def twitter
    # get the twitter account id, from the dropdown or from the most recent auth attempt
    if cookies[:new_twitter_account_id].to_i > 0
      session[:twitter_account_id] = cookies[:new_twitter_account_id]
      cookies[:new_twitter_account_id] = nil
    elsif params['/create/step1'] && params['/create/step1'][:twitter_account_id] != nil
      session[:twitter_account_id] = params['/create/step1'][:twitter_account_id]
    end
    # if we've got an account id, go to the next step
    if session[:twitter_account_id] != nil
      redirect_to '/create/step2'
    end
    # otherwise create a new twitter account and ask them to auth
    @twitter_account = TwitterAccount.new
  end
  
  # step 2
  def facebook
    # deal with facebook stuff
    update_facebook_end_points
    # if we get the facebook id from the dropdown, go to the next step
    if params['/create/step2'] && params['/create/step2'][:authorized_facebook_spot_id] != nil
      session[:facebook_id] = params['/create/step2'][:authorized_facebook_spot_id]
      redirect_to '/create/step3'
    end
    # otherwise, create the new fbook user and start asking for perms
    fb_user = Facebooker::User.new(current_user.fb_user_id)
    fb_user.session = Facebooker::Session.create(FACEBOOK_API_KEY,FACEBOOK_API_SECRET)
  end
  
  # step 3
  def options
    # check predicates
    if session[:twitter_account_id] == nil
      redirect_to '/create/step1'
    end
    if session[:facebook_id] == nil
      redirect_to '/create/step2'
    end
    # if they've submitted a title and the other options
    # this is basically the same as create() so I'm probably Doin' It Rong.
    logger.info("Twitter account: " + session[:twitter_account_id].to_s)
    logger.info("Facebook ID: " + session[:facebook_id].to_s)
    if(params[:posting_configuration])
      @posting_configuration = PostingConfiguration.new(params[:posting_configuration])
      @posting_configuration.twitter_account_id = session[:twitter_account_id]
      @posting_configuration.authorized_facebook_spot_id = session[:facebook_id]
      @posting_configuration.user = current_user
      if @posting_configuration.save
        logger.info("worked")
        flash[:twitter_account_id] = session[:twitter_account_id]
        flash[:facebook_id] = session[:facebook_id]
        session[:twitter_account_id] = nil
        session[:facebook_id] = nil
        redirect_to '/create/step4'
      else
        logger.info("failed")
        logger.info(@posting_configuration.errors)
        for e in @posting_configuration.errors.full_messages do
          logger.info("Problem: " + e.to_s)
          flash[:error] = e
        end
      end
    else
      @posting_configuration = PostingConfiguration.new()
      # check that we have offline access
      @offline_access = true
      fb_user = Facebooker::User.new(current_user.fb_user_id)
      unless fb_user.has_permission?('offline_access')
        @offline_access = false
      end
      # set the default title
      twitter_name = TwitterAccount.find_by_id(session[:twitter_account_id]).twitter_real_name
      facebook_name = AuthorizedFacebookSpot.find_by_id(session[:facebook_id]).title
      @default_title = twitter_name + " to " + facebook_name
    end
  end
  
  # step 4
  def complete
    # congratulations.
    if (flash[:twitter_account_id] && flash[:facebook_id])
      twitter_account = TwitterAccount.find_by_id(flash[:twitter_account_id]);
      @twitter_username = twitter_account.twitter_username
      @twitter_name = twitter_account.twitter_real_name
      facebook_entity = AuthorizedFacebookSpot.find_by_id(flash[:facebook_id]);
      if facebook_entity.page_type == 'page'
        @facebook_entity_type = 'Page'
      else
        @facebook_entity_type = 'Profile'
      end
      @facebook_name = facebook_entity.title
    end
  end
  
  # cancel multi-step
  def cancel
    session[:twitter_account_id] = nil
    session[:facebook_id] = nil
    redirect_to settings_path
  end
  
  def new

    redirect_to '/create/step1'

    # @user = current_user    
    # update_facebook_end_points
    # session[:posting_configuration] = nil
    # @twitter_account = TwitterAccount.new
    # @posting_configuration = PostingConfiguration.new
    # if cookies[:new_twitter_account_id].to_i > 0
    #   @posting_configuration.twitter_account_id = cookies[:new_twitter_account_id] 
    #   cookies[:new_twitter_account_id] = nil
    # end
    # if cookies[:new_fb_id].to_i > 0
    #   @posting_configuration.authorized_facebook_spot_id = cookies[:new_fb_id]
    #   cookies[:new_fb_id] = nil
    # end
    # unless cookies[:title_saver].blank?
    #   @posting_configuration.title = cookies[:title_saver]
    #   cookies[:title_saver] = nil
    # end
    # fb_user = Facebooker::User.new(current_user.fb_user_id)
    # fb_user.session = Facebooker::Session.create(FACEBOOK_API_KEY,FACEBOOK_API_SECRET)
    # @offline_access = true
    # unless fb_user.has_permission?('offline_access')
    #   @offline_access = false
    # end
  end
  
  def create
    @twitter_account ||= TwitterAccount.new
    @posting_configuration = PostingConfiguration.new(params[:posting_configuration])
    @posting_configuration.user = current_user
    cookies[:title_saver] = nil
    if @posting_configuration.save
      flash[:notice] = 'Connection was successfully created.'
      redirect_to settings_path
    else
      render :action => "new"
    end
  end
  
  def update
    @posting_configuration = PostingConfiguration.find(params[:id])
    @twitter_account = TwitterAccount.new
    cookies[:title_saver] = nil
    if @posting_configuration.has_valid_twitter_account?
      if @posting_configuration.update_attributes(params[:posting_configuration])
        flash[:notice] = 'Posting Configuration was successfully updated.'
        redirect_to settings_path
      else
        render :action => "edit" 
      end
    else
      flash[:error] = "You must grant access through OAuth for the Twitter user @#{@posting_configuration.twitter_account.twitter_username}"
      render :action => "edit"         
    end
  end
  
  def edit
    @user = current_user
    @posting_configuration = PostingConfiguration.find(params[:id])
    unless @posting_configuration.user == current_user
      flash[:error] = "You can not edit a connection you do not own."
      redirect_to("/settings")
    end
    update_facebook_end_points
    session[:posting_configuration] = @posting_configuration.id
    @twitter_account = TwitterAccount.new
    if cookies[:new_twitter_account_id].to_i > 0 #if the cookie is blank the int conversion will be 0
      @posting_configuration.twitter_account_id = cookies[:new_twitter_account_id] 
      cookies[:new_twitter_account_id] = nil
    end
    if cookies[:new_fb_id].to_i > 0
      @posting_configuration.authorized_facebook_spot_id = cookies[:new_fb_id] 
      cookies[:new_fb_id] = nil
    end
    unless cookies[:title_saver].blank?
      @posting_configuration.title = cookies[:title_saver]
      cookies[:title_saver] = nil
    end
    fb_user = Facebooker::User.new(current_user.fb_user_id)
    fb_user.session = Facebooker::Session.create(FACEBOOK_API_KEY,FACEBOOK_API_SECRET)
    @offline_access = true
    unless fb_user.has_permission?('offline_access') and @posting_configuration.post_as_notes?
      @offline_access = false
    end
    flash[:from_edit] = params[:id]
  end
  
  def disable
    @posting_configuration = PostingConfiguration.find(params[:id])
    unless @posting_configuration.user == current_user
      flash[:error] = "You can't disable a connection you do not own."
    else
      @posting_configuration.disable_tweetface = true
      @posting_configuration.save
      flash[:notice] = "Connection '#{@posting_configuration.title}' has been disabled."
    end
    redirect_to("/settings")
  end
  
  def enable
    @posting_configuration = PostingConfiguration.find(params[:id])
      unless @posting_configuration.user == current_user
        flash[:error] = "You can't enable a connection you do not own."
      else
        #check validity of twitter account choice
        if @posting_configuration.has_valid_twitter_account? 
          @posting_configuration.disable_tweetface = false
          @posting_configuration.save
          twitter_account = @posting_configuration.twitter_account
          twitter_account.polling = true
          twitter_account.save
          flash[:notice] = "Connection '#{@posting_configuration.title}' has been enabled."
        else
          flash[:error] = "You must edit the posting configuration and grant access through OAuth for the Twitter user @#{@posting_configuration.twitter_account.twitter_username} before you can enable it."
        end
     end
     redirect_to("/settings")    
  end
  
  def oauth
    #if there is an id then the call to oauth is coming from the edit action otherwise it is the new action
    session[:posting_configuration_id] = params[:id]
    @client = client
    request_token = @client.request_token(:oauth_callback => TWITTER_CALLBACK_URL)
    session[:request_token] = request_token.token
    session[:request_token_secret] = request_token.secret
    redirect_to request_token.authorize_url
  end
  
  def oauthback
    unless params[:denied].blank?
      flash[:error] = 'You denied OAuth access to your account.'
    else
      @client = client
      access_token = @client.authorize(session[:request_token],session[:request_token_secret],:oauth_verifier => params[:oauth_verifier])
      
      @client2 = TwitterOAuth::Client.new(
           :consumer_key => TWITTER_CONSUMER_KEY,
           :consumer_secret => TWITTER_CONSUMER_SECRET,
           :token => access_token.token, 
           :secret => access_token.secret
      )
      begin 
        new_twitter_acct = @client2.info
        if new_twitter_acct["error"].blank?
          @ta = TwitterAccount.find_by_user_id_and_twitter_user_id(current_user.id, new_twitter_acct["id"])
          if @ta
            @ta.oauth_token = access_token.token
            @ta.oauth_verifier = access_token.secret
            @ta.twitter_username = new_twitter_acct["screen_name"]
            @ta.twitter_picture = new_twitter_acct["profile_image_url"]
            @ta.twitter_real_name = new_twitter_acct["name"]
            @ta.twitter_private = new_twitter_acct["protected"],
            @ta.oauthed_at = Time.now
            @ta.polling = true
          else
            @ta = TwitterAccount.new(:user_id => current_user.id, 
            :twitter_user_id => new_twitter_acct["id"], 
            :twitter_username => new_twitter_acct["screen_name"],
            :twitter_real_name => new_twitter_acct["name"],
            :twitter_picture => new_twitter_acct["profile_image_url"],        
            :twitter_private => new_twitter_acct["protected"],
            :oauth_token => access_token.token,
            :oauth_verifier => access_token.secret,
            :oauthed_at => Time.now,
            :polling => true)
          end
          if @ta.save
            @ta.disable_non_oauthed_accounts if new_twitter_acct["protected"] == true
            #disable any legacy posting configurations from other users accessing it without OAuth
            # -- current users other posting configs with this private user should still work,
            # -- any other user using the twitter account that has oauthed it should not be affected
            cookies[:new_twitter_account_id] = @ta.id 
            #flash[:notice] = 'Twitter account was successfully authorized with OAuth.'
          else
            if @ta.errors[:twitter_user_id].blank?
              flash[:error] = 'The Twitter account had an error saving'
            else
              flash[:error] = @ta.errors[:twitter_user_id]
            end
          end
        else
          flash[:error] = "Twitter returned an error code from their API, you should try again later."
        end
      rescue JSON::ParserError
        flash[:error] = "The Twitter API is not responding properly at the moment. It is most likely over capacity. Please try to add your Twitter Source via OAuth again soon."        
      end
    end
    
    unless flash[:error].blank?
      redirect_to '/create/step2' 
    else
      if (flash[:from_edit])
        redirect_to '/posting_configurations/' + flash[:from_edit] + '/edit'
      else
        redirect_to '/create/step1'
      end
    end
    
    #    if session[:posting_configuration_id].blank?
    #      redirect_page = new_posting_configuration_path
    #    else
    #       @posting_configuration = PostingConfiguration.find_by_id(session[:posting_configuration_id])
    #       redirect_page = edit_posting_configuration_path(@posting_configuration)
    #    end
    #    session[:posting_configuration_id] = nil
    #    redirect_to redirect_page and return
  end
  
  
  def client
    @client = TwitterOAuth::Client.new(
         :consumer_key => TWITTER_CONSUMER_KEY,
         :consumer_secret => TWITTER_CONSUMER_SECRET
    )
  end
  
  def update_facebook_end_points
    # get ids of current facebook auth-ed spots for current_user
    
    fb_user = Facebooker::User.new(current_user.fb_user_id)
    fb_user.session = Facebooker::Session.create(FACEBOOK_API_KEY,FACEBOOK_API_SECRET)
    if fb_user.has_permission?('offline_access')
      #need to set this session so we are authorized to do these calls
      facebook_session = Facebooker::Session.new(FACEBOOK_API_KEY,FACEBOOK_API_SECRET)
      facebook_session.secure_with!(current_user.fb_session_key, current_user.fb_user_id)
      page_vals = facebook_session.fql_query("select page_id from page_admin where uid = #{current_user.fb_user_id}")
      # logger.info "page_admin_for_the: #{page_vals.inspect} -------------------page admin return val from fql query"
      if page_vals.count
        page_ids = []
        page_vals.map { |p| page_ids << p.page_id }
        unless page_ids.empty?
          fql_ids = "('"+page_ids.join("','")+"')"
          page_perm_vals = facebook_session.fql_query("select uid from permissions where publish_stream = 1 and uid in #{fql_ids}")
          if page_perm_vals.count
            while(fb_page = page_perm_vals.pop) do
              # logger.info "FB PAGE !!!!!!!!!!!!!!! #{fb_page.inspect} -- #{fb_page['uid']}"
              authed_spot = AuthorizedFacebookSpot.find_by_user_id_and_fb_id(current_user.id,fb_page['uid'])
              fb_page_fql = facebook_session.fql_query("select name, pic_square, page_url from page where page_id = #{fb_page['uid']}").pop
              # logger.info "FB PAGE INFO !!!!!!!!!!!! #{fb_page_fql.inspect}"
              
              if authed_spot
                authed_spot.title = fb_page_fql.name
                authed_spot.fb_url = fb_page_fql.page_url
                authed_spot.fb_picture = fb_page_fql.pic_square
                authed_spot.page_type = "page"        
                authed_spot.save if authed_spot.changed?
              else
                #create new authed_spot
                authed_spot = AuthorizedFacebookSpot.create(
                 :user_id => current_user.id, 
                 :title => fb_page_fql.name,
                 :fb_url => fb_page_fql.page_url,
                 :fb_picture => fb_page_fql.pic_square,
                 :page_type => "page",          
                 :fb_id => fb_page['uid']
                )
                cookies[:new_fb_id] = authed_spot.id
              end
            end # (while fb_page = page_perm_vals.pop)
          end
        end # (unless page_ids.empty?)
      end
      # check that we have perms for users profile (same fql query as pages perm but use user_id instead of page_id)    
      perm_val = fb_user.session.fql_query("select publish_stream from permissions where uid = '#{current_user.fb_user_id}'").pop
      # logger.info "publish_stream: #{perm_val.inspect} -------------------perms return val from fql query"
      if perm_val['publish_stream'] == "1"
        authed_spot = AuthorizedFacebookSpot.find_by_user_id_and_fb_id(current_user.id,current_user.fb_user_id)
        if authed_spot
          #possibly update values
          authed_spot.title = facebook_session.user.name
          authed_spot.fb_url = facebook_session.user.profile_url
          authed_spot.fb_picture = facebook_session.user.pic_square
          authed_spot.page_type = "profile"        
          authed_spot.save if authed_spot.changed?      
        else
          #create new authed_spot
          authed_spot = AuthorizedFacebookSpot.create(
          :user_id => current_user.id, 
          :title => facebook_session.user.name,
          :fb_url => facebook_session.user.profile_url,
          :fb_picture => facebook_session.user.pic_square,
          :page_type => "profile",          
          :fb_id => current_user.fb_user_id
          )
          cookies[:new_fb_id] = authed_spot.id
        end
      end  
      #
      # may even want to return true if the user has access to a page or their prfile that we don't have perms to, 
      # false otherwise, can use to show or not show the link to add new facebook end points
    end
    
  end 
end
