class UsersController < ApplicationController 
  #rescue_from ActionView::TemplateError, :with => Proc.new { redirect_to "/logout" and return }
  
  def create
    logout_keeping_session!
    redirect_to signup_closed_path
#    @user = User.new(params[:user])
#    success = @user && @user.save
#    if success && @user.errors.empty?
#            # Protects against session fixation attacks, causes request forgery
#      # protection if visitor resubmits an earlier form using back
#      # button. Uncomment if you understand the tradeoffs.
#      # reset session
#      self.current_user = @user # !! now logged in
#      redirect_back_or_default('/')
#      flash[:notice] = "Thanks for signing up!  We're sending you an email with your activation code."
#    else
#      flash[:error]  = "We couldn't set up that account, sorry.  Please try again, or contact an admin (link is above)."
#      render :action => 'new'
#    end
  end
  
  def edit
    @user = self.current_user    
    #@user = self.current_user = User.first #TODO: change, this is only in here to get a current_user after all the changes
    if @user.nil?
      redirect_to '/logout'
      return
    end
    if @user.posting_configurations.empty?
      #redirect_to new_posting_configuration_path
      redirect_to '/create/step1'
    end
    @offline_access = true
    unless @user.fb_user.has_permission?('offline_access')
      @offline_access = false
    end

    @page_title = "#{@user.name}"
  end

  def update
    @user = self.current_user
    params[:user][:twitter_name].gsub!("@","")
    if @user.update_attributes(params[:user])
      if params[:commit] == "Tweet in your Face!"
        if self.current_user.follow
          flash[:notice] = 'TweetPo.st has been re-enabled.'
        else
          flash[:notice] = 'TweetPo.st had an error enabling. Try again in a moment.'
        end
      else
        flash[:notice] = 'Account was successfully updated.'
      end
      redirect_to(settings_path)
    else
      render :action => "edit"
    end
  end
  
  def link_user_accounts
    if self.current_user.nil?
      redirect_to signup_closed_path    
#      #register with fb
#      User.create_from_fb_connect(facebook_session.user)
    else
      #connect accounts
      @user = self.current_user
      @user.fb_session_key = facebook_session.session_key
      @user.save
      self.current_user = @user
      self.current_user.link_fb_connect(facebook_session.user.id) unless self.current_user.fb_user_id == facebook_session.user.id
      redirect_to settings_path
    end
  end
  
  def enable
    if self.current_user.follow
      flash[:notice] = 'TweetPo.st has been re-enabled.'
    else
      flash[:notice] = 'TweetPo.st had an error enabling. Try again in a moment.'
    end
    redirect_to settings_path
  end
  
  def disable
    if self.current_user.unfollow
      flash[:notice] = 'TweetPo.st has been disabled.'
    else
      flash[:notice] = 'TweetPo.st had an error disabling. Try again in a moment'
    end  
    redirect_to settings_path
  end
  
  def signup_closed
    
  end
end
