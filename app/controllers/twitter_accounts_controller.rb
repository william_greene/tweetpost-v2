class TwitterAccountsController < ApplicationController
  
  def create
    @twitter_account = TwitterAccount.new(params[:twitter_account])
    @posting_configuration = PostingConfiguration.find_by_id(session[:posting_configuration])
    session[:posting_configuration] = nil
    @twitter_account.twitter_username = @twitter_account.twitter_username[1..-1] if @twitter_account.twitter_username[0] == 64 #64 is ascii code for @   
    @twitter_account.user = current_user
    
    client = TwitterOAuth::Client.new(
         :consumer_key => 'TWITTER_CONSUMER_KEY',
         :consumer_secret => 'TWITTER_CONSUMER_SECRET'
     )

     begin
        authorized_user = client.show(@twitter_account.twitter_username)
        if authorized_user["error"] #If the user was not found then do not update attributes
          flash[:error] = "Twitter Source '#{@twitter_account.twitter_username}' could not be found."
          redirect_to '/create/step1' and return
        elsif authorized_user["protected"] == true
          flash[:error] = "The Twitter Source '#{@twitter_account.twitter_username}' is private, please use the 'Authorize TweetPo.st' button above."
          redirect_to '/create/step1' and return
        else
          @twitter_account.update_attributes(:twitter_user_id => authorized_user["id"], :twitter_real_name => authorized_user["name"], :twitter_picture => authorized_user["profile_image_url"])
        end
    
        if @twitter_account.save
          session[:twitter_account_id] = @twitter_account.id
          redirect_to '/create/step2' and return
#          flash[:notice] = 'Twitter Source was successfully created.'
#          if @posting_configuration
#            redirect_to(edit_posting_configuration_path(@posting_configuration))
#          else
#            redirect_to(new_posting_configuration_path)
#          end
        else
          flash[:error] = 'You have already added that Twitter Source.'
          redirect_to '/create/step1' and return
        end
     rescue JSON::ParserError
       flash[:error] = "The Twitter API is not responding properly at the moment. It is most likely over capacity. Please try to add your Twitter Source again soon."
       redirect_to '/create/step1' and return
     end       
  end
end