# Filters added to this controller apply to all controllers in the application.
# Likewise, all the methods added will be available for all controllers.

class ApplicationController < ActionController::Base
  helper :all # include all helpers, all the time
  protect_from_forgery

  include AuthenticatedSystem

  layout 'index'
  before_filter :set_facebook_session
  helper_method :facebook_session
  
  rescue_from Facebooker::Session::SessionExpired, :with => :facebook_session_expired
  rescue_from Facebooker::Session::MissingOrInvalidParameter, :with => :facebook_session_error
  # rescue_from Curl::Err::RecvError,  :with => :face_book_curl_error
  rescue_from SocketError, :with => :facebook_session_error
  
  def face_book_curl_error
    flash[:error] = "Facebook encountered a problem, please log in again"
    reset_session
    redirect_back_or_default('/')
  end
  
  def facebook_session_expired
    clear_fb_cookies!
    clear_facebook_session_information
    reset_session # remove your cookies!
    flash[:error] = "Your facebook session has expired, please log in again!"
    redirect_back_or_default('/')
  end
  
  def facebook_session_error
    flash[:error] = "Facebook encountered a problem, please log in again"
    reset_session
    redirect_back_or_default('/')
  end
  
  def splash
    #redirect_to "http://blog.thesnowballfactory.com/2010/02/18/tweetpo-st-the-next-generation/"
  end

  # Scrub sensitive parameters from your log
  # filter_parameter_logging :password
end
