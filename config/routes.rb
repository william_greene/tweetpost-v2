ActionController::Routing::Routes.draw do |map|
  map.logout '/logout', :controller => 'sessions', :action => 'destroy'
  map.login '/login', :controller => 'sessions', :action => 'new'
  map.register '/register', :controller => 'users', :action => 'create'
  map.signup '/signup', :controller => 'users', :action => 'new'
  map.signup_closed '/signup_closed', :controller => 'users', :action => 'signup_closed'
  map.resources :users, :collection => {:link_user_accounts => :get}, :member => {:enable => :get, :disable => :get}
  map.root :controller => "sessions", :action => "new"
  map.resources :session
  map.resources :posting_configurations
  map.resources :authorized_facebook_spots 
  map.resources :twitter_accounts   
  map.settings '/settings', :controller => "users", :action => "edit"
  map.enable_post_config '/enable_post_config/:id', :controller => 'posting_configurations', :action => "enable"
  map.disable_post_config '/disable_post_config/:id', :controller => 'posting_configurations', :action => "disable"
  map.oauth '/oauth', :controller => 'posting_configurations', :action => 'oauth'
  map.oauthback '/oauthback', :controller => 'posting_configurations', :action => 'oauthback'  
  map.splash '/splash', :controller => "application", :action => "splash"
  map.step1 '/create/step1', :controller => 'posting_configurations', :action => 'twitter'
  map.step2 '/create/step2', :controller => 'posting_configurations', :action => 'facebook'
  map.step3 '/create/step3', :controller => 'posting_configurations', :action => 'options'
  map.step4 '/create/step4', :controller => 'posting_configurations', :action => 'complete'
  map.cancel '/create/cancel', :controller => 'posting_configurations', :action => 'cancel'
end
