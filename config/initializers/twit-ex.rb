TWITTER_REGEX = %r{
 (                          # leading text
   <\w+.*?>|                # leading HTML tag, or
   [^=!:'"/]|               # leading punctuation, or
   ^                        # beginning of line
 )
 (
   (?:https?://)|           # protocol spec, or
   (?:www\.)                # www.*
 )
 (
   [-\w]+                   # subdomain or domain
   (?:\.[-\w]+)*            # remaining subdomains or domain
   (?::\d+)?                # port
   (?:/(?:(?:[~\w\+%-]|(?:[,.;@:][^\s$]))+)?)* # path
   (?:\?[\w\+%&=.;:-]+)?    # query string
   (?:\#[\w\-\.]*)?         # trailing anchor
 )
 ([[:punct:]]|\s|<|$)       # trailing text
}x