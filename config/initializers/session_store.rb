# Be sure to restart your server when you modify this file.

# Your secret key for verifying cookie session data integrity.
# If you change this key, all old sessions will become invalid!
# Make sure the secret is at least 30 characters and all random, 
# no regular words or you'll be exposed to dictionary attacks.
ActionController::Base.session = {
  :key         => '_tweetpost_session',
  :secret      => '567b00eb371e0271824ba8c8da86c22f05812e7b13c44e21be49dbd615e3bee49156bfec33ae18bbba1c6ab870ef88234d861fe66fb73432f5aee37d9595b8ce'
}

# Use the database for sessions instead of the cookie-based default,
# which shouldn't be used to store highly confidential information
# (create the session table with "rake db:sessions:create")
# ActionController::Base.session_store = :active_record_store
