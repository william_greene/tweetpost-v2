def retry_until(condition, options = {}, &block)
  opts = { :tries => 1 }.merge(options)
  retries = opts[:tries]

  begin
    result = yield
    raise
  rescue
    retry if (retries -= 1) > 0 && !condition.call(result)
	return result
  end

  yield
end
