set :application, "tweetface"

set :scm, :git
set :repository, "git@cloudspace.unfuddle.com:cloudspace/#{application}.git"
set :branch, "master"
set :deploy_via, :remote_cache
set :git_enable_submodules, true


set :user, "root"
ssh_options[:keys] = ["#{ENV['HOME']}/.ssh/id-gsg-keypair-snowball"]
ssh_options[:forward_agent] = true
set :use_sudo, false



# PLEASE PULL FIRST TO GET RECENT UPDATES
# and then CHECK THE SERVER URLs LISTED BELOW BEFORE DEPLOYING
role :web,      "tweetpo.st"
role :app,      "tweetpo.st"
role :db,       "tweetpo.st", :primary => true
  
# role :web,      "tweetpo.st"
# role :app,      "tweetpo.st"
# role :db,       "tweetpo.st", :primary => true
 
set :deploy_to, "/srv/#{application}"
 
set :rails_env, "production"
 
 
namespace :deploy do
  desc "Restarting mod_rails with restart.txt"
  task :restart, :roles => :app, :except => { :no_release => true } do
    run "sudo touch #{current_path}/tmp/restart.txt"
  end
  
  [:start, :stop].each do |t|
    desc "#{t} task is a no-op with mod_rails"
    task t, :roles => :app do ; end
  end
end

task :fix_permissions do
  run "sudo chown -R www-data:www-data /srv/#{application}"
end
after "deploy:update_code", :fix_permissions

namespace :gems do
  desc "Install gems"
  task :install, :roles => :app do
    run "sudo cd #{current_path} && #{sudo} rake RAILS_ENV=production gems:install"
  end
end
