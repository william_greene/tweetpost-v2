// Place your application-specific JavaScript functions and classes here
// This file is automatically included by javascript_include_tag :defaults
/*
function hideContent() {
	if (document.getElementById('posting_configuration_awesm_powered').checked == 1){
		document.getElementById('enabled-clicked').style.display = 'block';
	} else {
		document.getElementById('enabled-clicked').style.display = 'none';
	}
}
*/
$(document).ready(
	function(){
		$('#posting_configuration_awesm_powered').click(function(){
			$('#awesm').show(100);
		});
	}
);

function popUpAjax(id) {
  $('#warningBox').css("display", 'block');
  $('#flash_notices').css("display", 'none');
}


function createCookie(name,value,days) {
	if (days) {
		var date = new Date();
		date.setTime(date.getTime()+(days*24*60*60*1000));
		var expires = "; expires="+date.toGMTString();
	}
	else var expires = "";
	document.cookie = name+"="+value+expires+"; path=/";
}

function readCookie(name) {
	var nameEQ = name + "=";
	var ca = document.cookie.split(';');
	for(var i=0;i < ca.length;i++) {
		var c = ca[i];
		while (c.charAt(0)==' ') c = c.substring(1,c.length);
		if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length,c.length);
	}
	return null;
}

function eraseCookie(name) {
	createCookie(name,"",-1);
}

function saveTitle(){
    createCookie("title_saver",document.getElementById('posting_configuration_title').value,1);
}

function saveSource(){
	var obj = document.getElementById('posting_configuration_twitter_account_id');
	var index = obj.selectedIndex;
	var selected_option_value = obj.options[index].value;
    createCookie("new_twitter_account_id",selected_option_value,1);	
}

function saveDest(){
	var obj = document.getElementById('posting_configuration_authorized_facebook_spot_id');
	var index = obj.selectedIndex;
	var selected_option_value = obj.options[index].value;
    createCookie("new_fb_id",selected_option_value,1);	
}