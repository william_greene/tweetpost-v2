module Awesm
  def awesmate(uri, user_params = {})
    parameters = {
      "version" => 1,
      "target" => uri,
      "api_key" => "0d67a7c385c7c6854bfa12307092556b880a7fca12e599c8ae10de91eeaf8d86",
      "share_type" => "facebook-status",
      "create_type" => "tweetpostv2"
    }
    
    parameters.merge! user_params
    case response = make_awesm_call(parameters)
    when Net::HTTPSuccess
      return Hash.from_xml(response.body)['url']
    when Net::HTTPClientError
      parameters['api_key'] = '0d67a7c385c7c6854bfa12307092556b880a7fca12e599c8ae10de91eeaf8d86'
      response = make_awesm_call(parameters)
      if Net::HTTPSuccess
        return Hash.from_xml(response.body)['url']
      else
        uri
      end
    else
      uri
    end
  end
  
  protected
    def make_awesm_call parameters
      response = Net::HTTP.start('create.awe.sm') do |http|
        request = Net::HTTP::Post.new('/url.xml')
        request.form_data = parameters
        http.request(request)
      end      
    end
end
