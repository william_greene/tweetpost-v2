while true do
  tweets = Tweet.find_all_by_state('received')
  unless tweets.empty?
    puts "\n#{Time.now} Processing #{tweets.count} tweets."

    tweets.each do |tweet|
      puts "\n#{Time.now} #{tweet.twitter_user_id}/#{tweet.status_id}: #{tweet.status[0..60]}"

      tweet.process!
      if tweet.twitter_accounts.empty?
        tweet.fail!
        # TODO: fail into no twitter account found
        puts "\n#{Time.now} #{tweet.twitter_user_id}/#{tweet.status_id}: no matching twitter accounts found"
        next
      else
        tweet.twitter_accounts.each do |account|
          # TODO: put disabled handling inside
          puts account.inspect
          puts account.enabled_configs.inspect
          account.enabled_configs.each { |config| config.post tweet }
          if account.enabled_configs.empty?
            puts "\n#{Time.now} #{tweet.twitter_user_id}/#{tweet.status_id}: no enabled posting configurations found"
            next
          end
        end
        puts "\n#{Time.now} #{tweet.twitter_user_id}/#{tweet.status_id}: Processed"
        tweet.deliver!
      end
    end
    # 
    # 
    #   rescue Timeout::Error
    #     puts "ERROR: TIMEOUT ERROR - We have a timeout error from facebook, trying to apply prefs to tweet object again"
    #     tweet.apply_preferences
    #     puts e.message.to_s
    #     puts e.backtrace
    #   # rescue RunTime::Error => e
    #   #   tweet.apply_preferences
    #   #   puts "We have a runtime error"
    #   #   puts e.message.to_s
    #   #   puts e.backtrace
    #   rescue StandardError => e
    #     puts "ERROR: STANDARD ERROR - We have a standard error, tweet state set to 'failed'"
    #     tweet.fail!
    #     tweet.save
    #     puts e.inspect
    #     puts e.message.to_s
    #     puts e.backtrace
    #   end
    # end
  else
    puts "no tweets found to be in the state 'received', nothing to do, ho hum"
    puts "imma rest here for a bit - sleeping 10 seconds\n"
    sleep 10
  end
end
