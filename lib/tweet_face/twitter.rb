require 'rubygems'
require 'tweetstream'
require 'activerecord'

module TweetFace
  class TwitterConnection
    def self.run!
     twitter_user_ids = TwitterAccount.all.collect { |t| t.twitter_user_id }.uniq
      TweetStream::Client.new(TWITTER_MASTER_USER,TWITTER_MASTER_PASS).follow(*twitter_user_ids) do |status|
         Tweet.create_from status
      end
      # latest = Tweet.all(:conditions => { :state => "delivered" }, :limit => 1, :order => "status_id DESC").first.status_id
      # twitter = Twitter::Base.new(Twitter::HTTPAuth.new(TWITTER_MASTER_USER, TWITTER_MASTER_PASS))
      # twitter.friends_timeline(:since_id => latest).collect { |mash| Tweet.create_from mash }
    end
  end
end






# 
# 
# require 'rubygems'
# require 'tweetstream'
# require 'activerecord'
# 
# def last_user_made_at
#   TwitterAccount.last.created_at
# end
# 
# while true
#   users = TwitterAccount.all.collect { |t| t.twitter_user_id }.uniq.join(', ')
#   $last_user_made_at = last_user_made_at
#   
#   TweetStream::Client.new('tweetpo_sttest', '20dvd56').on_delete{ |status_id, user_id|
#         del_tweet = Tweet.find_by_status_id(status_id)
#         del_tweet.deleted_on_twitter! if del_tweet.state == 'received'
#       }.on_limit { |skip_count|
#         # do something
#       }.follow(users) do |status|
#   
#     Tweet.create_from status
#   
#     break if last_user_made_at > ($last_user_made_at + 2.minutes)
#     
#     # When a network error (TCP/IP level) is encountered, back off linearly. Perhaps start at 250 milliseconds, double, and cap at 16 seconds. 
#     # Network layer problems are generally transitory and clear quickly.
#     # 
#     # When a HTTP error (> 200) is returned, back off exponentially. Perhaps start with a 10 second wait, double on each subsequent failure, 
#     # and finally cap the wait at 240 seconds. Consider sending an alert to a human operator after multiple HTTP errors, as there is probably 
#     # a client configuration issue that is unlikely to be resolved without human intervention. There's not much point in polling any faster 
#     # in the face of HTTP error codes and your client is may run afoul of a rate limit.
#     #
#     # For example, collect "raw" statuses (that is, not parsed or marshaled into your language's native object format) in one process, 
#     # and pass each status into a queueing system, rotated flatfile, or database. In a second process, consume statuses from your queue 
#     # or store of choice, parse them, extract the fields relevant to your application, etc. Consumers of high-volume streams 
#     # should consider performing JSON and XML markup parsing in a parallel manner as the status volume is approaching the single processor 
#     # throughput limit of some software stacks. End-to-end stress test your stack.
#   end
# 
# end
# 
# # write a cronjob that runs every 3 minutes and checks to see if this file is running
# # if it is not running, have it notify us that it is restarting it and start it running again
# # only check every 3 minutes so we are not restarting it over and over again
# # may also want to put in a check to make sure that we are not restarting it because it died more than 4 times in any hour