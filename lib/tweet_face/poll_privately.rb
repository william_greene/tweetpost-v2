require 'twitter'

while true
    start = Time.now
  
    twitter_accounts = TwitterAccount.all(
                      :conditions => 'twitter_accounts.polling = 1 AND 
                                      posting_configurations.disable_tweetface = 0',
                      :include => :posting_configurations, :order => 'twitter_accounts.created_at DESC').uniq
  
    twitter_accounts.each do |account|
      puts "Trying #{account.twitter_username}\n\n"
      begin 
        if account.has_oauth_tokens?
          client = TwitterOAuth::Client.new(:consumer_key => TWITTER_CONSUMER_KEY, :consumer_secret => TWITTER_CONSUMER_SECRET, :token => account.oauth_token, :secret =>account.oauth_verifier)
        else
          client = Twitter::Base.new(Twitter::HTTPAuth.new(TWITTER_MASTER_USER, TWITTER_MASTER_PASS)) 
        end
      rescue StandardError => e
        puts "ERROR ON TWITTER CONNECTION for #{account.inspect} | has_oauth_tokens? = #{account.has_oauth_tokens?}"
        puts e.message.to_s
        puts e.backtrace        
        next
      end
      # using an inflated count to guarantee we get a users valid but older tweets that may have occurred within last 15 minutes
      # because if they enter the streaming api we may have got a new one, but not their other recent ones
      # this is why we are not using the since_id as the twitter api docs strongly recommend when continuously polling specific users
      params = { :user_id => account.twitter_user_id, :count => 50 }
      if account.twitter_private == 1 && !account.new?
        params[:since_id] = account.last_tweet 
        params.delete :count
      end
      puts "#{account.twitter_username} TIMELINE CALL: #{params.inspect}\n"
      begin
        tweetmash = client.user_timeline(params)
      rescue Twitter::InformTwitter, Errno::ECONNRESET, Twitter::Unavailable, Timeout::Error, JSON::ParserError
        sleep 5
        retry
      rescue Twitter::Unauthorized
        next  
      end
      tweetmash = Array[tweetmash] if tweetmash.class == Hash
    
      # reversing the list to start with oldest tweets first (users want their oldest tweets delivered first, better chance of that this way)
      tweetmash.reverse.each do |status|

        unless status.has_key? "error"
          puts "\ntext: #{status["text"]} \nid: #{status["id"]} | status_created #{status["created_at"]} by: #{status["user"]["name"]}| now #{Time.now}"
          tweet = Tweet.find_by_status_id(status["id"])
          unless tweet      
            tweet = Tweet.create do |t|
              t.status_id = status["id"]
              t.twitter_user_id = status["user"]["id"]
              t.reply_to = status["in_reply_to_user_id"]
              t.status = HTMLEntities.new.decode(status["text"])
              t.source = "polling"
              t.status_created_at = status["created_at"]
              puts "tweet object created"
            end

            # note: the 15 minute guard may cause missed tweets for slightly older tweets (if this loop starts to take more than 15 minutes)          
            if Time.parse(status["created_at"]) < 15.minutes.ago
              puts "TWEET is older than 15 minutes, status filtered - status time #{status['created_at']} | now #{Time.now}"
              tweet.dead_on_arrival!
            end
          
          else
            puts "not creating: tweet already in system - tweet status id #{status['id']}"
            if tweet.source == 'streaming' 
              unless account.oauthed_at.nil?
                if Time.parse(status['created_at']) > account.oauthed_at
                  puts "\n\nNEW TWEET FOUND IN STREAMING for account with oauth date- disabling account - #{account.inspect}\n"
                  account.polling = false
                  account.save
                  puts "disabling completed - #{Time.now}\n"                
                end
              else
                puts "\n\nNEW TWEET FOUND IN STREAMING for account with no oauth date- disabling account - #{account.inspect}\n"
                account.polling = false
                account.save
                puts "disabling completed - #{Time.now}\n"              
              end
            end
          end
        else
          puts "#{Time.now} skipping this user #{account.twitter_username}, returned error: #{status['error']}\nfull return value: #{status.inspect}"
          break
        end
      end    
    end

    puts "Run - start #{start} | end #{Time.now}"  
    puts "Sleeping #{sleep_time = (twitter_accounts.count > 200 ? 0 : 1.minutes.to_i)}"
    sleep sleep_time
end
