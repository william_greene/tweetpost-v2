include SelectivelyLog

while true do
  puts "#{Time.now} - going for a run."
    
  posts = FacebookPost.all(:conditions => "state != 'failed' and state != 'delivered' and state != 'previewing_attempt3' and state != 'publishing_attempt3' AND MOD(id,2)=0")
#  posts = FacebookPost.all(:conditions => "state != 'received' and state != 'failed' and state != 'delivered' and state != 'previewing_attempt3' and state != 'publishing_attempt3'")
  unless posts.empty?
    loadlog
    puts "\n#{Time.now} Processing #{posts.count} posts."
    posts.each { |post| post.publish! }
  else
    sleep 10
  end
  sleep 10
  
end
