include SelectivelyLog

while true do
  puts "#{Time.now} - checking for links that need previews."

  posts = FacebookPost.all(:conditions => "state='previewing_attempt0' OR state='previewing_attempt1' OR state='previewing_attempt2'")
  unless posts.empty?
    puts "\n#{Time.now} Processing #{posts.count} previews."
    posts.each { |post| post.publish_links! }
  else
    sleep 10
  end
  sleep 10
  
end