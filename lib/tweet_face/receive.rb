require 'json'
require 'mq'

SERVER_QUEUE = 'streamtweets'

puts "Let's process some tweets! Yeehaw Junction!"
Signal.trap('INT') { AMQP.stop{ EM.stop } }
Signal.trap('TERM'){ AMQP.stop{ EM.stop } }
AMQP.start(:host => 'localhost', :user => 'guest', :insist => true) do

  #AMQP.logging = true
  MQ.prefetch(1)
  queue = MQ.queue(SERVER_QUEUE, :durable => true)
  puts "connected to queue"  
  queue.subscribe(:ack => true) do |h, hash|
    #h.ack
    puts "#{Time.now} next! message: #{hash}"
    begin
      status = JSON.parse(hash)
    rescue TypeError, JSON::ParserError
      puts "\n\nSTRANGE ERROR _ POSSIBLE BUG (why did a non json object make it into the queue)\nmessage fetched from queue is of non standard type, could not parse as json, ack-ing this message"
      h.ack
      next
    end
    if status.has_key? "text" and status.has_key? "id" and status.has_key? "user" and status['user'].has_key? "id" and status.has_key? "in_reply_to_user_id"
        tweet = Tweet.find_by_status_id(status['id'])
        unless tweet 
          tweet = Tweet.create do |t|
            t.status_id = status['id']
            t.twitter_user_id = status['user']['id']
            t.reply_to = status['in_reply_to_user_id']
            t.status = HTMLEntities.new.decode(status['text'])
            t.status_created_at = status["created_at"]            
            t.source = "streaming"
            puts "tweet object created"
          end
        else
          puts "tweet object not created, matching tweet found in system" 
        end
    else
      puts "\n\nSTRANGE ERROR _ POSSIBLE BUG (is it a streaming api message type we need to respond to)\nmessage hash missing one of these required keys: id, user['id'], in_reply_to_user_id, text"
    end
    h.ack
  end
end

#tweetpost posts your tweets to your facebook account
# Tweets.each do |tweet|
#   tweet.process!
#   UserMappings.find_by_twitter_id(tweet.user).each do |mapping|
#     mapping.facebook.post(mapping.twitter.get)
#   end
#   tweet.deliver!
# end
