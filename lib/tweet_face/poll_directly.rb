require 'twitter'

twitter = Twitter::Base.new(Twitter::HTTPAuth.new(TWITTER_MASTER_USER, TWITTER_MASTER_PASS))
PostingConfiguration.all(:conditions => ['updated_at > ?', 20.minutes.ago]).each do |configuration|
  configuration.collect(&:twitter_account).collect(&:twitter_user_id).uniq.each do |user_id|
    user_tweet = Tweet.find(:first, :conditions => [ "twitter_user_id = ?", user_id ])
    if user_tweet
      count = '10'
    else
      count = '1'
    end
    tweetmash = twitter.user_timeline({:user_id => user_id, :count => #{count}})
    tweetmash.each do |status|
      if status[:created_at] newer than 5 minutes before posting_config created_at
        tweet = Tweet.find_by_status_id(status[:id])
        unless tweet
          tweet = Tweet.create do |t|
            t.status_id = status[:id]
            t.twitter_user_id = status.user[:id]
            t.reply_to = status[:in_reply_to_user_id]
            t.status = HTMLEntities.new.decode(status[:text])
            puts "tweet object created"
          end
        else
          #break to avoid processing older tweets for this user
          break
          puts "not creating: tweet already in system"
        end
      end
    end
  end
end
