class TweetFace::FacebookConnection
  def self.run!
    Tweet.all(:conditions => { :state => 'received' }).collect do |tweet|
      begin
        tweet.process!
        tweet.apply_preferences
      rescue StandardError
        tweet.fail!
        tweet.save
      end
    end
  end
end