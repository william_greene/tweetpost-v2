#!/usr/bin/env ruby
require 'rubygems'
require 'active_support'
require 'mq'
require 'tweetstream'
require 'sequel'

# monkeypatching tweetstream library
# (actually Twitter:JSONStream which is used by tweetstream)
# printing all data received with timestamp
# useful to know that we are stll connected to the streaming api
module Twitter
  class JSONStream < EventMachine::Connection
    def parse_stream_line ln
puts ln+' time:'+Time.now.to_s
      ln.strip!
      unless ln.empty?
        if ln[0,1] == '{'
          @each_item_callback.call(ln) if @each_item_callback
        end
      end
    end
  end
end

$db = Sequel.connect('mysql://tweetface:allhailuma1r@tweetpo.st/tweetface_production')
# $db = Sequel.connect('mysql://root@localhost/tweetface_development', :socket =>'/Applications/XAMPP/xamppfiles/var/mysql/mysql.sock')
# $db = Sequel.connect('mysql://jeremiah:jeremiah@localhost/tweetface_development', :socket => '/var/run/mysqld/mysqld.sock')

$last_stream_update = Time.now

@status = Proc.new do |status|
  puts "\n    stream| status : #{status.inspect}\n"
  @twitter.publish status.to_json
  STDOUT.flush
end

def attemptable?
  15.minutes.ago > $last_stream_update
end

def needs_update?
  config_change = $db[:posting_configurations].
                  select(:updated_at).
                  reverse_order(:updated_at).
                  limit(1).
                  first[:updated_at]
  $last_stream_update < config_change
end

def gather_ids
  $ids = $db[:twitter_accounts].
           select(:twitter_user_id).
           join(:posting_configurations, :twitter_account_id => :id).
           distinct.
           filter(:disable_tweetface => false).
           map &:values
  puts "collected ids #{$ids * ','}"
  $ids
end

def refresh_stream
  puts "setting stream follow ids: #{gather_ids * ','} #{Time.now}"
  $last_stream_update = Time.now
  @stream.filter :follow => gather_ids, :count => 150000, &@status
  #  @stream.filter :follow => gather_ids, &@status
end

def start_clients
  @amq = MQ.new(AMQP.connect(:host => 'localhost', :user => 'guest'))
  @twitter = @amq.queue('streamtweets')
  #@stream = TweetStream::Client.new('tweet_post','allhailuma1r', :yajl)
  @stream = TweetStream::Client.new('tweet_post','tw33t1Ny0urF4C3', :yajl)
  # @stream = TweetStream::Client.new('jeremiahleetest','password09', :yajl)
end

#AMQP.logging = true

while true do
  EventMachine.run do
    start_clients
    refresh_stream
    
    @stream.on_error do |message|
      puts "\n\n!STREAMEVENT: #{Time.now}on error : #{message}\n\n"
      STDOUT.flush
    end

#    stream.on_delete do |message|
#      puts "\n\n!STREAMEVENT: on delete : #{message}\n\n"
#    end

#    stream.on_limit do |message|
#      puts "\n\n!STREAMEVENT: on limit : #{message}\n\n"
#    end

#    @stream.on_reconnect do |timeout, retries|
#      puts "\n\n!STREAMEVENT: reconnecting in: #{timeout} seconds. (#{retries} retries)\n\n"
#      STDOUT.flush
#    end

    EM.add_periodic_timer(8) do
      if needs_update? and attemptable?
        puts "PERIODIC TIMER STRIKES! - refresh_stream #{Time.now}"
        refresh_stream
      end
    end    
  end
end
