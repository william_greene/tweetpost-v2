module SelectivelyLog
  require 'logger'
  
  def slog(usr, msg)
  	# selected_users = [1,2]
  	# Obtain via: select user_id, twitter_username, twitter_user_id from twitter_accounts where twitter_username = "jeremiahlee";
  	selected_users = [0,20,43,8677,724,9594,2733,2733,6200,14389]
  	
    if selected_users.include?(usr)
      selective_log_file = File.open('/srv/tweetface/current/log/selective.txt', 'a')
      # selective_log_file = File.open('/home/jeremiah/projects/gitrepo2/tweetface/log/selective.txt', 'a')
      
      selective_log = Logger.new(selective_log_file, 'weekly')
      selective_log.debug "User #{usr} at #{Time.now}: #{msg}"
      selective_log.close
    end
    
  end
  
  def loadlog
  	load_log_file = File.open('/srv/tweetface/current/log/load.txt', 'a')
  	# load_log_file = File.open('/home/jeremiah/projects/gitrepo2/tweetface/log/load.txt', 'a')
    load_log = Logger.new(load_log_file, 'weekly')
    
    load_log.debug "\n Load as of #{Time.now}:"

    load_query = ActiveRecord::Base.connection.execute('select state, count(*) from facebook_posts group by state')
    while row = load_query.fetch_row do
      load_log.debug "    #{row[0]}: #{row[1]}"
    end
    load_query.free
  
    load_log.close
  end

end
